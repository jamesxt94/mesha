

import {Socket as webSocket} from './WebSocketBrowser.ts'
import { ClientSocket} from './ClientSocket.ts'
import * as async from 'gh+/kwruntime/std@1.1.4/util/async.ts'
import {Exception} from 'gh+/kwruntime/std@1.1.4/util/exception.ts'

export class Client{

	static async fromSocket(socket: webSocket){
		let client = new ClientSocket()
		client.socket = socket
		client.init()
		return client 

	}

	static async connect(address: string){
		let def = new async.Deferred<any>()
		let socket: webSocket
		if(address.startsWith("ws://") || address.startsWith("wss://")){
			socket = new webSocket()
		}
		else{
			throw Exception.create("Only ws: and wss: protocol supported").putCode("INVALID_PROTOCOL")
		}
		socket.once("error", def.reject)
		socket.once("connect", def.resolve) 
		socket.connect(address)
		
		await  def.promise
		let client = new ClientSocket()
		client.socket = socket
		client.init()
		return client 
	}

	static connectLocal(cid: string){
		throw Exception.create("Only ws: and wss: protocol supported").putCode("INVALID_PROTOCOL")
	}

}
