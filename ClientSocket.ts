import uniqid from 'https://esm.sh/uniqid@5.4.0'
import { EventEmitter} from 'events'
//import linebyline from 'https://esm.sh/linebyline@1.3.0'


import {AddressInfo, Socket} from 'net'
import {Socket as webSocket} from './WebSocket.ts'


export var MeshaJSON = Symbol("mesha-json")

export interface Metadata{
    type: string
    address: AddressInfo | string 
    http?: {
        address: AddressInfo | string 
        enabled: boolean
    }
}

export class ClientSocket extends EventEmitter{

    id = uniqid()
    meta: Metadata 
    socket: Socket | webSocket
    jsonOptions = {
        reviver: null,
        replacer: null
    }
    binaryTransfer = false 
    $bcount = 0
    $waiting = null
    $buffers = new Map<string, Buffer>()
    $msgs = []
    $rl: any //readline.Interface
    $writequeue = []


    queue(msg: any){
        if(!msg.data){
            console.info(msg)
        }
        this.emit("message", msg.data)
    }


    get writable(){
        return this.socket.writable && (!this.socket.destroyed) && (!this.socket.writableEnded)
    }


    $parse(cmd: string){
        let self = this 
        try{
            return JSON.parse(cmd, function(key: string, value: any){
                return self.$transformReviver(this, key, value)
            })
        }catch(e){
            return {
                "$mesha_error": {
                    message: e.message,
                    code: e.code || e.type 
                }
            } 
        }
    }

    $stringify(cmd: any, replacer?: any){
        let self = this
        return JSON.stringify(cmd, function(key: string, value: any){
            return self.$transformReplacer(this, replacer, key, value)
        })
    }

    $transformReviver(self: any, key: string, value: any){
        if(value && value.type == "buffer"){
            if(value.data){
                value = Buffer.from(value.data, 'base64')
            }
            else if(value.ref){
                value = this.$buffers.get(value.ref)
                this.$buffers.delete(value.ref)
            }
        }
        if(this.jsonOptions.reviver){
            value = this.jsonOptions.reviver.call(this, key, value)
        }
        return value
    }

    $transformReplacer(self:any, replacer,  key: string, value: any){


        if(self[key] && (typeof self[key][MeshaJSON] == "function")){
            value = self[key][MeshaJSON]()
        }


        if(Buffer.isBuffer(self[key])){

            if(this.binaryTransfer && (self[key].length > 4096)){
                let params = {
                    id: this.id + "." + (this.$bcount++),
                    len: self[key].length
                }

                let q:any = {
                    clientId: this.id,
                    data: params,
                    type: 'write'
                }
                this.$writequeue.push(Buffer.from(this.$stringify(q) + "\n"))
                this.$writequeue.push(self[key])
                this.$writequeue.push(Buffer.from("\n"))
                value = {
                    type: 'buffer',
                    ref: params.id
                }
            }else{
                value = {
                    type:'buffer',
                    data: self[key].toString('base64')
                }
            }
        }
        if(this.jsonOptions.replacer){
            value = this.jsonOptions.replacer.call(this, key, value)
        }
        if(replacer){
            value = replacer.call(this, key, value)
        }
        return value
    }


    send(msg: any, replacer: any = null, type= undefined){
        //let def = new async.Deferred<any>()
        let q:any = {
            clientId: this.id,
            data: msg
        }
        if(type) q.type = type 
        let str = Buffer.from(this.$stringify(q, replacer) + "\n")
        
        if(this.$writequeue.length){
            let buffers = this.$writequeue
            this.$writequeue = []
            for(let buffer of buffers){
                this.socket.write(buffer)
            }
        }


        if(this.socket.write (str) === false){
            this.socket.pause()
        }
        return 
        /*
        this.socket.write(str, function(er){
            if(er) return def.reject(er)
            def.resolve()
        })
        return def.promise 
        */
    }


    $read(bytes: Buffer){
        if(this.$waiting){
            this.$waiting.data.push(bytes)
            this.$waiting.downloaded += bytes.length 
            //console.info(this.$waiting.downloaded, this.$waiting.len)
            if(this.$waiting.downloaded > this.$waiting.len){
                this.$buffers.set(this.$waiting.id, Buffer.concat(this.$waiting.data).slice(0, this.$waiting.len))
                this.$waiting = null 
            }
        }

        else{
            let cmd = this.$parse(bytes.toString())
            if(cmd && cmd.$mesha_error){
                console.error("[mesha] Failed getting message:", cmd.$mesha_error.message, "Code:", cmd.$mesha_error.code)
            }
            else if(cmd){
                if(cmd.data && (cmd.type == "write")){
                    cmd = cmd.data
                    cmd.data = []
                    cmd.downloaded = 0
                    this.$waiting = cmd
                }
                else{
                    this.queue(cmd)
                }             
            }
            else{
                console.error("[mesha] Failed getting message:", bytes.toString())
            }
        }
    }
    

    stopRPCMode(){
        this.socket.removeAllListeners()
        this.removeAllListeners()
    }

    init(){

        if(typeof this.socket.setKeepAlive == "function"){
            this.socket.setKeepAlive(true, 5000)
        }

        this.socket.on("error", (e)=> this.emit("socket-error", e))
        this.socket.on("close", (e)=> {
            this.emit("close")
            if(this.$rl){
                this.$rl.close()
            }
            this.socket.removeAllListeners()
            delete this.socket 
        })

        
        let buffers = [], readline = null 
        if(readline){
            this.$rl = readline.createInterface({
                input: this.socket,
            }).on("line", this.$read.bind(this))
        }
        else{
            this.socket.on("data", (bytes) => {

                while(true){
                    let i = bytes.indexOf(10)
                    if(i >= 0){
                        buffers.push(bytes.slice(0, i + 1))
                        let data = Buffer.concat(buffers)
                        buffers = []
                        this.$read(data)
                        bytes = bytes.slice(i + 1)
                    }
                    else{
                        break 
                    }
                }
                if(bytes.length){
                    buffers.push(bytes)
                }
            })
        }

        /*
        this.$rl = linebyline (this.socket, {
            maxLineLength: 10*1024*1024, // 10MB
            retainBuffer: true
        })
        this.$rl.on("line", this.$read.bind(this))
        */
       
        this.socket.on("drain",()=>{
            this.socket.resume()
        })
       
    }
}
