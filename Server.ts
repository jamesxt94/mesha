import { ClientSocket, MeshaJSON} from './ClientSocket.ts'
import {EventEmitter} from 'events'
import net from 'net'
import * as async from 'gh+/kwruntime/std@1.1.4/util/async.ts'
import {Exception} from 'gh+/kwruntime/std@1.1.4/util/exception.ts'
import Path from 'path'
import os from 'os'
import fs from 'fs'
import {Client} from './Client.ts'
import crypto from 'crypto'
import {WSocketToTCPServer} from 'gitlab://jamesxt94/codes@1627dc8f/socket2tcp/server.ts'

import  {Server as KwHttpServer} from "github://kwruntime/std@1.1.19/http/server.ts"
import {JsonParser} from 'github://kwruntime/std@1.1.19/http/parsers/json.ts'
import  {Router} from "github://kwruntime/std@1.1.19/http/router.ts"
import  {HttpContext} from "github://kwruntime/std@1.1.19/http/context.ts"
import { Writable } from 'stream'
import { ServerResponse } from 'http'


export interface StreamInfo{
    deferred: async.Deferred<Writable>,
    client_deferred?: async.Deferred<void>,
    query?: {[key: string]: any}
}

export class Server extends EventEmitter{

    #h_server: KwHttpServer
    #h_router: Router 
    #local_address = ''
    #stream_promises = new Map<string, StreamInfo>()    
    $net: net.Server

    constructor(){
        super()
        this.$net = net.createServer(this.$listener.bind(this))
    }


    get http(){
        return this.#h_server
    }

    /*
    async bindWebSocket(params: any){
        let addr = this.$net.address()
        if(!addr){
            addr = await this.bind("tcp://127.0.0.1:0")
        }
    }
    */

    async bind(address: string){
        let def = new async.Deferred<any>()
        this.$net.once("error", def.reject)
        this.$net.once("listening", def.resolve) 

        if(address.startsWith("unix://")){
            address = address.substring(7)
        }
        if(address.startsWith("/") || address.startsWith("\\")){
            // unix socket, named pipe?
            if(os.platform() != "win32"){
                try{
                    let client = await Client.connect(address)
                    client.socket.destroy()
                }catch(e){
                    if((e.code == "ECONNREFUSED") || (e.code == "ENOENT")){
                        // delete file 
                        if(fs.existsSync(address))
                            fs.unlinkSync(address)
                    }
                    else{
                        throw e 
                    }
                }
            }
            this.$net.listen(address)
        }
        if(address.startsWith("tcp://")){
            let parts = address.substring(6).split(":")
            this.$net.listen(Number(parts[1]), parts[0])
        }

        if(address.startsWith("ws://") || address.startsWith("wss://")){
            // start first a tcp local
            // and convert to wss 
            let addr = await this.bind("tcp://127.0.0.2:0")
            if(addr?.port){
                this.#local_address = `tcp://127.0.0.2:${addr.port}`
                // convert to WEBSOCKET
                await this.#createHttpServer(address.replace(/wss?\:/, "tcp:"))
                return this.#h_server.address
            }
        }
        await def.promise
        return this.$net.address()
    }

    startLocal(cid: string){
        let addr = ''
        if(os.platform() == "win32"){
            addr = "\\\\.\\pipe\\" + crypto.createHash('md5').update(cid).digest("hex")
        }
        else{
            let file = Path.join(os.homedir(), ".kawi", "sockets")
            if(!fs.existsSync(file)) fs.mkdirSync(file)
            file = Path.join(file, cid)
            addr = "unix://" + file
        }
        return this.bind(addr)
    }

    $listener(socket: net.Socket){

        let client = new ClientSocket()
        client.meta = {
            type: 'server',
            address: this.$net.address(),
            http: {
                enabled: Boolean(this.#h_server),
                address: this.#h_server?.address
            }
        }
        client.socket = socket 
        client.init()
        this.emit("client", client)
    }


    async #createHttpServer(address: string){

        let httpserver = new KwHttpServer()
        httpserver.bodyParsers.set("application/json", JsonParser)
        let router = this.#h_router = new Router()
        router.catchNotFound = true 
        router.catchUnfinished = true 
        router.put("/stream/add", async (context) => {
            
            let query = Object.assign(context.request.query || {}, context.request.body || {})
            if(!query.id){
                throw Exception.create("Invalid id sent").putCode("INVALID_ID")
            }

            let deferred = new async.Deferred<Writable>()
            this.#stream_promises.set(query.id, {
                deferred,
                query
            })
            return this.#apiSend(context, {
                ok: true,
                id: query.id
            })
        })

        router.put("/stream/data", async (context) => {
            let query = Object.assign(context.request.query || {}, context.request.body || {})
            if(!query.id){
                throw Exception.create("Invalid id sent").putCode("INVALID_ID")
            }
            let def = this.#stream_promises.get(query.id), ok = false
            if(!def){
                throw Exception.create(`Stream with id  ${query.id} not available`).putCode("STREAM_NOT_AVAILABLE")
            }
            
            context.reply.raw.on("close", () => {
                if(!ok) def.deferred.reject(Exception.create(`Request closed, stream incomplete`).putCode("STREAM_INCOMPLETE"))
            })

            let stream = await def.deferred.promise
            let d1: async.Deferred<void>
            stream.on("close", function(){
                if(d1) d1.resolve()
            })
            if(stream["socket"]){
                stream["socket"].on("close", function(){
                    if(d1) d1.resolve()
                })
            }
            for await (let chunk of context.request.raw){
                if(stream["socket"].destroyed) break 
                if(!stream.writable || stream.writableEnded){
                    break
                }
                if(stream.write(chunk) === false){
                    d1 = new async.Deferred<void>()
                    stream.once("drain", d1.resolve)
                    await d1.promise
                }
            }
            stream.end()
            //context.request.socket.destroy()
            return this.#apiSend(context, null)
        })

        router.get("/stream/get", async (context) => {
            let query = Object.assign(context.request.query || {}, context.request.body || {})
            try{                
                if(!query.id){
                    throw Exception.create("Invalid id sent").putCode("INVALID_ID")
                }
                let def = this.#stream_promises.get(query.id)
                if(!def){
                    throw Exception.create(`Stream with id  ${query.id} not available`).putCode("STREAM_NOT_AVAILABLE")
                }
                
                let headers = def.query.header || []
                for(let header of headers){
                    let i = header.indexOf("=")
                    let name = header.substring(0, i)
                    let value = header.substring(i+1)
                    context.reply.raw.setHeader(name, value)
                }
                let def1 = def.client_deferred = new async.Deferred<void>()
                await def.deferred.resolve(context.reply.raw)
                await def1.promise

                if(!context.reply.raw.writableEnded){
                    throw Exception.create("Stream data get failed").putCode("STREAM_FAILED")
                }
            }catch(e){
                throw e
            }
            finally{
                this.#stream_promises.delete(query.id)
            }

        })

        router.get("/", function(context){
            return context.reply.send({
                msg: "Mesha service started",
                status: 'ok'
            })
        })

        
        this.#h_server = httpserver
        await httpserver.listen(address)
        let w = new WSocketToTCPServer({
            password: "TCP"
        })
        /*w.on("path", (ev) => {
            ev.address = this.#local_address
        })*/
        w.maps.set("connect", this.#local_address)
        w.maps.set("service", this.#local_address)
        w.setHttpServer(httpserver.raw)
        w.createWebSocket()
        
        let write = ServerResponse.prototype.write
        ServerResponse.prototype.write = function(data, callback){
            if(!this.writable || this.writableEnded){
                if(callback) callback()
                return 
            } 
            return write.apply(this, arguments)
        }
        this.#httpBeginAccept()
    }


    #apiSend(context:HttpContext, data:any, status?: number){
		if(status) context.reply.code(status)
		return context.reply.header("content-type","application/json;charset=utf-8").send(data)
	}

    async #httpBeginAccept(){
        let iterator = this.#h_server.getIterator(["error","request"])
		for await(let event of iterator){
			try{
				if(event.type == "error"){
					console.error("> Error from webserver:", event.data.message)
				}
				else {
					this.#h_router.lookup(event.data)
				}
			}catch(e){
				console.error("> Failed to accept requests:", e.message)
			}
		}
    }

}
