import{Writable as W, Readable as R} from 'stream'


export class Readable extends R{
	id?: string 
}

export class Writable extends W{
	id?: string 
}
