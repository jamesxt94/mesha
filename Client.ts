import Path, { join } from 'path'
import os from 'os'
import fs from 'fs'
import crypto from 'crypto'

import {Socket} from 'net'
import {Socket as webSocket} from './WebSocket.ts'
import { ClientSocket} from './ClientSocket.ts'

import * as async from 'gh+/kwruntime/std@1.1.4/util/async.ts'

export class Client{

    static async fromSocket(socket: Socket | webSocket){
        let client = new ClientSocket()
        client.socket = socket
        client.init()
        return client 

    }

    static async connect(address: string){
        let socket = await this.$connectSocket(address)
        let client = new ClientSocket()
        client.socket = socket
        client.meta = {
            type: 'client', 
            address,
            http: {
                enabled: address.startsWith("ws://") || address.startsWith("wss://"),
                address: (address.startsWith("ws://") || address.startsWith("wss://")) ? address.replace(/(wss?)\:/, (_, a) => `http${a == "wss" ? "s" : ""}:`).split("/").slice(0, 3).join("/") : ''
            }
        }
        client.init()
        return client 
    }

    static async $connectSocket(address: string){
        let def = new async.Deferred<any>()
        let socket: Socket | webSocket
        if(address.startsWith("ws://") || address.startsWith("wss://")){
            socket = new webSocket()
        }
        else{
            socket = new Socket()
        }
        socket.once("error", def.reject)
        socket.once("connect", def.resolve) 
        if(address.startsWith("unix://")){
            address = address.substring(7)
        }
        if(address.startsWith("/") || address.startsWith("\\") || address.startsWith("ws://") || address.startsWith("wss://")){
            // unix socket, named pipe?
            socket.connect(address)
        }
        if(address.startsWith("tcp://")){
            let parts = address.substring(6).split(":")
            socket.connect(Number(parts[1]), parts[0])
        }
        await  def.promise
        return socket 
    }

    static connectLocal(cid: string){
        let addr = ''
        if(os.platform() == "win32"){
            addr = "\\\\.\\pipe\\" + crypto.createHash('md5').update(cid).digest("hex")
        }
        else{
            let file = Path.join(os.homedir(), ".kawi", "sockets")
            if(!fs.existsSync(file)) fs.mkdirSync(file)
            file = Path.join(file, cid)
            addr = "unix://" + file
        }
        return this.connect(addr)
    }

    static $connectLocalSocket(cid: string){
        let addr = ''
        if(os.platform() == "win32"){
            addr = "\\\\.\\pipe\\" + crypto.createHash('md5').update(cid).digest("hex")
        }
        else{
            let file = Path.join(os.homedir(), ".kawi", "sockets")
            if(!fs.existsSync(file)) fs.mkdirSync(file)
            file = Path.join(file, cid)
            addr = "unix://" + file
        }
        return this.$connectSocket(addr)
    }


}
