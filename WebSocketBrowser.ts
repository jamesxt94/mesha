// emulate a net.Socket from WebSockets
import { EventEmitter } from 'events'
import {Exception} from "gh+/kwruntime/std@1.1.4/util/exception.ts"


export class Socket extends EventEmitter{
	$int = null 
	w3c: WebSocket
	$buf = []


	constructor(){
		super()
		
	}
	connect(address: string){
		this.w3c = new window.WebSocket(address, "echo-protocol")
		this.w3c.binaryType = "arraybuffer"

		this.w3c.onopen = ()=> this.emit("connect")
		this.w3c.onerror = (e)=> {			
			let ex = Exception.create("Connect error.").putCode(e.type)
			ex.event = e 
			this.emit("error", ex)
		}
		
		this.w3c.onclose = ()=> this.emit("close")
		this.w3c.onmessage = async (e)=> {
			let data = e.data 
			if(data instanceof Blob){
				let index = this.$buf.length
				this.$buf[index] = null 
				data = await e.data.arrayBuffer()
				this.$buf[index] = Buffer.from(data)
				this.$emitData()
			}else{
				this.emit("data", Buffer.from(data))
			}
		} 
	}


	$emitData(){
		if(this.$buf.indexOf(null) >= 0){
			return 
		}
		let bytes = Buffer.concat(this.$buf)
		this.$buf.splice(0, this.$buf.length)
		this.emit("data", bytes)
	}

	write(data){

		this.w3c.send(data)
		return 
		
		// For unknown reason websocket is closed on bigger messages
		let len = 64 * 1024
		while(data.length){
			let bytes = data.slice(0, len)
			this.w3c.send(bytes)
			data = data.slice(len)
		}
	}

	end(data){
		if(data)
			this.w3c.send(data)
		
		this.w3c.close()
	}

	// pause and resume not available?
	pause(){}
	resume(){}
	setKeepAlive(enable: boolean, time: number){
		
		//disable for now
		return 

		if(this.$int){
			clearTimeout(this.$int)
		}
		if(!enable) return 

		if(time > 0){
			time = Math.min(time, 5000)
		}
		let ping = ()=>{
			if(this.destroyed){
				delete this.$int 
				return 
			}
			this.write("{}\n")
			this.$int = setTimeout(ping, time)
		}
		this.$int = setTimeout(ping, time)
	}

	get writable(){
		return this.w3c.readyState == this.w3c.OPEN
	}

	get writableEnded(){
		return this.destroyed
	}

	get destroyed(){
		return this.w3c.readyState == this.w3c.CLOSED
	}

	destroy(){
		this.w3c.close()
	}
}
