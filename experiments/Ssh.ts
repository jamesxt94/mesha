import {Client as SSHClient} from 'npm://ssh2@1.5.0'
import {ClientSocket} from './Messaging.ts'
import Path from 'path'
import os from 'os'
import fs from 'fs'
import * as async from "gh+/kwruntime/std@1.1.0/util/async.ts"
import net from 'net'


export interface SshOptions{
    
    host:  string
    username: string 
    passwod?: string 
    port?: number 
    privateKey?: string 
}

// Make available a client through SSH
export class Client{


    static async connect(options: SshOptions, cid: string){

        /*

        let file = Path.join(os.homedir(), ".kawi", "sockets")
        if(!fs.existsSync(file)) fs.mkdirSync(file)
        file = Path.join(file, cid)
        return this.connect("unix://" + file)
        */

        let def = new async.Deferred<void>()
        let conn = new SSHClient()
        conn.on("ready", def.resolve)
        conn.on("error", def.reject)
        conn.connect(options)
        await def.promise

        let def1 = new async.Deferred<any>()
        conn.sftp((err, sftp) => {
            if (err) return def.reject(err)
            def1.resolve(sftp)
        })
        let sftp = await def1.promise 


        let wstream = sftp.createWriteStream(`~/.mesha-${cid}.ts`)
        def = new async.Deferred<void>()
        wstream.end(`import {Client} from "${import.meta.url}"
        Client.$start(${JSON.stringify(cid)})
        `)
        wstream.on("error", def.reject)
        wstream.on("finish", def.resolve)

        conn.exec(`PATH=/usr/KwRuntime/bin:~/KwRuntime/bin:$PATH kwrun ~/.mesha-${cid}.ts`, (err, stream)=>{
            if(err) return def1.reject(err)
            return def1.resolve(stream)
        })
        let stream = await def1.promise 


        def = new async.Deferred<void>()
        let func = null 
        func = function(b){
            if(b.toString().indexOf("[mesha] Socket redirect started") >= 0){
                def.resolve()
                def = null 
            }
            else{
                stream.once("data", func)
            }
        }
        stream.once("close", (e)=> {
            if(def) def.reject(e)
        })
        stream.once("data", func)
        await def.promise
                

        let client = new ClientSocket()
        client.socket = stream
        client.init()
        return client 

    }

    static async $start(cid: string){

        let buffers = [], func 
        process.stdin.on("data", (b)=> {
            if(func){
                func(b)
            }
            else{
                buffers.push(b)
            }
        })


        let file = Path.join(os.homedir(), ".kawi", "sockets")
        if(!fs.existsSync(file)) fs.mkdirSync(file)
        file = Path.join(file, cid)
        let address = "unix://" + file
        let def = new async.Deferred<any>()

        let socket = new net.Socket()
        socket.once("error", def.reject)
        socket.once("connect", def.resolve) 

        if(address.startsWith("unix://")){
            address = address.substring(7)
        }
        if(address.startsWith("/")){
            // unix socket, named pipe?
            socket.connect(address)
        }
        if(address.startsWith("tcp://")){
            let parts = address.substring(6).split(":")
            socket.connect(Number(parts[1]), parts[0])
        }
        await  def.promise
        console.info("[mesha] Socket redirect started")

        // pipe socket to stdout and stdin
        func = (b) => socket.write(b)
        if(buffers.length){
            func(Buffer.concat(buffers))
            buffers = []
        }        
        socket.on("error", (e)=> process.stderr.write("[Socket error] " + String(e.message || e)))
        socket.on("data", (b)=> process.stdout.write(b))
        socket.on("close", ()=> process.exit(1))
    }

}
