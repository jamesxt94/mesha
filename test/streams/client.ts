import {Client} from '../../Client.ts' 
import {RPC} from '../../RPC.ts'
import fs from 'fs'
import * as async from "gh+/kwruntime/std@1.1.19/util/async.ts"

let shared = {
	sum(a,b){
		return a + b
	}
}

main()
async function main(){
	try{
		let client = await Client.connect("tcp://mda.kodhe.work:8083")
		const rpc = new RPC()
		rpc.channel = client  
		rpc.init()	
		let Helper = await rpc.defaultScope.remote("Helper")
		console.info("Sum:", await Helper.sum(12, 20))
		console.info("Sum:", await Helper.sum(25, 32))

		let def = new async.Deferred<void>()
		let sw = await rpc.createWriteStream()
		//let sr = fs.createReadStream(__filename)
		//sr.pipe(sw)
		//sr.once("error", def.reject)
		sw.on("error", function(e){
			console.error("Bad:", e)
		})
		sw.once("finish", def.resolve)

		setTimeout(async function(){
			let i = 0
			while(true){
				let def = new async.Deferred<void>()
				sw.write(`Times: ${i++} Date: ${Date.now()}`.repeat(4*1024*1024), (e) => e ? def.reject(e) : def.resolve())
				await def.promise
				await async.sleep(500)
			}

		}, 1000)

		let text= await Helper.read(sw.id)
		console.info("Text:",text)
		//await def.promise 		


		process.exit(0)
	}catch(e){
		console.error("[ERROR]", e.message)
		process.exit(1)
	}
}