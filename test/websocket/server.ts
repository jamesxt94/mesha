import {Server} from '../../Server.ts'
import {ClientSocket} from '../../ClientSocket.ts'
import {RPC} from '../../RPC.ts'
import {WSocketToTCPServer} from 'https://gitlab.com/jamesxt94/codes/-/raw/1627dc8/socket2tcp/server.ts'





main()
async function main(){
	let server = new Server()
	let shared = {
		sum(a,b){
			return a + b
		},
	
		async read(id: string){
			let w = await RPC.createReadStreamFromServer(server, id)
			let buffers = [], times = 0
			for await (let chunk of w){
				times++
				if(times == 8)
					await new Promise((a,b)=> setTimeout(a, 10000))
				console.info("Readed:", chunk.length)
				//buffers.push(chunk)
			}
			//return buffers.toString()
		}
	}

	server.on("error", console.error)

	server.on("client", (client:ClientSocket)=>{
		let rpc = new RPC()
		rpc.channel = client
		rpc.init()
		rpc.defaultScope.add("Helper", shared)
		
		client.on("close", ()=>{
			console.info("Closing client. Current scopes length:", rpc.$scopes.size)
			rpc = null
		})
	})

	let addr = await server.bind("wss://0.0.0.0:8082")
	console.info("WebSocket listening:", addr)

	/*
	// start on a local port
	let addr = await server.bind("tcp://127.0.0.1:0")
	console.info("TCP listening on:", addr)
	// redirect to websocket using socket2tcp
	let wsocketServer = new WSocketToTCPServer({
		address: "tcp://0.0.0.0:8082",
		password: "TCP"
	})
	wsocketServer.createHttpServer()
	wsocketServer.createWebSocket()
	wsocketServer.maps.set("service", "tcp://127.0.0.1:" + addr.port)
	let addr2 = await wsocketServer.listen()
	console.info("WebSocket listening on:", addr2)
	*/

}