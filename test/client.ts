import {Client} from '../Client.ts'
import {RPC} from '../RPC.ts'


let shared = {
	sum(a,b){
		return a + b
	}
}

main()
async function main(){
	let client = await Client.connectLocal("com.kodhe.mesha-test1")
	const rpc = new RPC()
	rpc.channel = client  
	rpc.init()	
	let Helper = await rpc.defaultScope.remote("Helper")
	console.info("Sum:", await Helper.sum(12, 20))
	console.info("Sum:", await Helper.sum(25, 32))
	process.exit(0)
}