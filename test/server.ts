import {Server} from '../Server.ts'
import {ClientSocket} from '../ClientSocket.ts'
import {RPC} from '../RPC.ts'


let shared = {
	sum(a,b){
		return a + b
	}
}

main()
async function main(){
	let server = new Server()
	server.on("error", console.error)
	server.on("client", (client:ClientSocket)=>{
		let rpc = new RPC()
		rpc.channel = client
		rpc.init()
		rpc.defaultScope.add("Helper", shared)
		client.on("close", ()=>{
			console.info("Closing client. Current scopes length:", rpc.$scopes.size)
			rpc = null
		})
	})
	let addr = await server.startLocal("com.kodhe.mesha-test1")
	console.info("Address:", addr)
}