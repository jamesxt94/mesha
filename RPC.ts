//import lodash from "https://unpkg.com/lodash-es@4.17.15/lodash.js"
import { ClientSocket, MeshaJSON } from "./ClientSocket.ts"
import util from 'util'
import * as async from 'gh+/kwruntime/std@1.1.0/util/async.ts'
import uniqid from 'https://esm.sh/uniqid@5.4.0'
import {Exception} from 'gh+/kwruntime/std@1.1.0/util/exception.ts'
import http, { IncomingMessage } from 'http'
import https from 'https'
import { Readable, Writable } from "./Stream.ts"
import { PassThrough } from "stream"
import { Client } from "./Client"
import { Socket } from "net"
import {kawix} from "github://kwruntime/core@b9d4a6a/src/kwruntime.ts"
//import Stream from 'stream'


kawix["_vars"] = kawix["_vars"] || {}
if(kawix["_vars"]["com.kodhe.mesha"] === undefined){
    kawix["_vars"]["com.kodhe.mesha"] = {}
}
const meshaVars = kawix["_vars"]["com.kodhe.mesha"]


let tid = uniqid()
let count = 0
let genid = function(){
    return tid + "-" + String(count++)
}

const SharedValueSymbol = Symbol("shared-value")
export var ShareSymbol = Symbol("share")

export interface Property{
    type: "field" | "property" | "method",
    name: string 
}


function isSharedObject(value: any): Boolean{
    if(value){
        let symbols = Object.getOwnPropertySymbols(value)
        for(let symbol of symbols){
            if(String(symbol) == "Symbol(share)"){
                return Boolean(value[symbol])
            }
        }
    }
    return false 
}



export class Scope{
    id: string 
    rpc: RPC
    $vars = new Map<string, any>()
    $tasks = new Map<string, any>()
    $jsonreplacer: any
    $remotes = new Map<string, any>()


    constructor(rpc, id?: string){
        this.rpc = rpc
        if(!id){
            id = "SC" + genid()
        }
        this.id = id 
    
        let self = this 
        this.$jsonreplacer = function(key: string, value: any){

            if(value){
                if(isSharedObject(value)){
                    value = self.rpc.shared(value)
                }
                
                if(value instanceof Shared){
                    value = self.$share(value.$value)
                }
            }
            return value 

        }
    }

    add(name: string, value: any){
        let p = {
            name,
            value,
            properties: this.rpc.$getProps(value)
        }
        this.$vars.set(name, p)
        return p
    }

    remove(name: string){
        this.$vars.delete(name)
    }

    get(name: string){
        let obj = this.$vars.get(name)
        if(obj){
            return obj.value 
        }   
    }

    props(name: string){
        let obj = this.$vars.get(name)
        if(obj){
            return obj.properties
        }
    }


    $share(value: any){
        
        let ckey = '', val = null
        for(let key of this.$vars.keys()){
            val = this.$vars.get(key)
            if(val.value === value){
                ckey = key 
                break 
            }
        }
        if(!ckey){
            ckey = "V-" + genid()
            val = this.add(ckey, value)
            
        }
        return {
            type: 'sharedobject',
            name: val.name,
            scope: this.id,
            properties: val.properties
        }
    }


    async remote(name: string, parent?: Target, props?: Array<Property>){

        if(parent){
            name = parent.$value.name + "." + name
        }

        let remote = this.$remotes.get(name)
        if(!remote){
            // read properties 
            if(!props){
                props = await this.call("__rpc.props", [name])
                if(!props){
                    throw Exception.create("Failed to get remote: "+ name).putCode("INVALID_REMOTE_OBJECT")
                }
            }

            remote = new Target(this, {
                name,
                properties: props
            })
            this.$remotes.set(name, remote)
        }
        return remote 
    }


    




    $remote(name: string, props?: Array<string>){
        let remote = this.$remotes.get(name)
        if(!remote){
            remote = new Target(this, {
                name,
                properties: props
            })
            this.$remotes.set(name, remote)
        }
        return remote 
    }


    $transform(data: any){

        if(data){
            if(data instanceof Shared){
                return this.$share(data)
            }
            if(typeof data == "object"){
                for(let id in data){
                    data[id] = this.$transform(data)
                }
            }    
        }
        return data  

    }

    destroy(){
        if(this.rpc && this.rpc.channel){
            if(this.rpc.channel.writable)
                this.call("__rpc.$remote_remove_scope", [this.id])
        }
        this.clear()
    }


    clear(){

        if(this.$tasks.size){
            let keys = this.$tasks.keys()
            let e = Exception.create("Scope was disposed").putCode("RPC_DESTROYED")
            for(let key of keys){
                this.$tasks.get(key).reject(e)
            }
        }

        if(this.$vars){
            this.$vars.clear()
            delete this.$vars
        }
        if(this.$tasks){
            this.$tasks.clear()
            delete this.$tasks
        }

        if(this.$remotes){
            this.$remotes.clear()
            delete this.$remotes
        }

        delete this.$jsonreplacer        
        if(this.rpc){
            this.rpc.$scopes.set(this.id, null)
            this.rpc.$scopes.delete(this.id)
            delete this.rpc
        }
    }

    get writable(){
        if(!this.rpc || !this.rpc.channel || !this.rpc.channel.writable) return false 
        return true 
    }

    async call(target: string, args: any[]){

        let cmd = {
            type: "req",
            scope: this.id,
            task: genid(),
            args,
            target
        }

        
        if(!this.writable)
            throw Exception.create("RPC socket was closed").putCode("RPA_DESTROYED") 

        let def = new async.Deferred<any>()
        this.$tasks.set(cmd.task, def)
        
        this.rpc.channel.send(cmd, this.$jsonreplacer)
        let result = await  def.promise 
        this.$tasks.delete(cmd.task)

        if(result.error){
            let e = Exception.create(result.error.message).putCode(result.error.code)
            e.originalStack = e.stack
            e.stack = result.error.stack 
            e.type = result.error.type 
            throw e 
        }
        else{
        
            return result.data

        }
    }
}

export class Shared{
    $value: any
    constructor(value){
        this.$value = value 
    }

    [util.inspect.custom](depth, options) {
        return `Shared => {innerObject=${util.inspect(this.$value, options)}}`
    }

}


const createFunc = Symbol("createFunc")
const prop = Symbol("prop")
const invoke = Symbol("invoke")
const invokeMember = Symbol("invokeMember")




export class Target{
    $value : any 
    $scope : Scope
    $fields: Map<string,any>
    invoke: Function 
    invokeMember: Function

    constructor(scope: Scope,value: any){
        this.$scope = scope 
        this.$value = value
        
        this[".prop"] = this[prop]
        this[".createFunc"] = this[createFunc]


        for(let prop of value.properties){
            if(prop.name == "invoke") prop.name = "invoke_2"
            if(prop.name == "invokeMember") prop.name = "invokeMember_2"
            if(prop.name.startsWith(".")) continue 
            //if(prop.name == "subItem") prop.name = "subItem"


            if(prop.type == "property" || prop.type == "field"){
                this[".createProp"](prop)
            }
            else{
                this[".createFunc"](prop)
            }
        }

        this.invoke = this[invoke]
        this.invokeMember = this[invokeMember]
        
    }


    [util.inspect.custom](depth, options) {
        let props = this.$value.properties.filter((a)=> a.type == "property").map((a)=> a.name)
        let fields = this.$value.properties.filter((a)=> a.type == "field").map((a)=> a.name)
        let methods = this.$value.properties.filter((a)=> a.type == "method").map((a)=> a.name)
        let p:any = {}
        if(props.length) p.props = props.join(", ")
        if(fields.length) p.fields = fields.join(", ")
        if(methods.length) p.methods = methods.join(", ")
        

        return `Target (RemoteObject ${this.$value.name}) => {Scope=${util.inspect(this.$scope.id, options)}, Memberinfo=${util.inspect(p, options)}}`
    }


    [createFunc](prop: Property){
        let self = this
        Object.defineProperty(this, prop.name, {
            value: function(){
                return self[invokeMember](prop.name, Array.from(arguments))
            }
        })
    }   

    [".createProp"](prop: Property){
        let self = this
        Object.defineProperty(this, prop.name, {
            get: function(){
                return self[invokeMember](prop.name, Array.from(arguments))
            }
        })

        Object.defineProperty(this,"get_" + prop.name, {
            value: function(){
                return self[invokeMember](prop.name, Array.from(arguments))
            }
        })
    }   

    [prop](name: string){
        return this.$scope.remote(name, this)
    }

    

    [invoke](...args: Array<any>){
        return this[invokeMember]("#invoke", args)
    }

    [invokeMember](name: string, args: Array<any>){
        return this.$scope.call(`${this.$value.name}.${name}`, args)        
    }

    [MeshaJSON](){
        return {
            type: "ownvariable",
            name: this.$value.name 
        }
    }

}




export class RPC{
    
    channel: ClientSocket
    $scopes = new Map<string, Scope>()    
    //static #streams = new Map<string, PassThrough>()

    static get #streams(){ 
        if(!meshaVars["streams"]){
            meshaVars["streams"] = new  Map<string, PassThrough>()
        }
        return meshaVars["streams"]        
    }


    static async createReadStreamFromServer(server: any, id: string): Promise<Readable>{
        let address = server.http?.address
        
        if(address){
            let url = '', puturl = ''
            if(address?.port){
                url = `http://127.0.0.1:${address.port}/stream/get`
                puturl = `http://127.0.0.1:${address.port}/stream/data`
            }
            else{
                url = `${address}/stream/get`
                puturl = `${address}/stream/data`
            }
            return await RPC.createReadStreamFromURL(url, id)
        }
        else{
            return RPC.createReadStream(id)
        }
    }

    static createReadStream(id: string){
        let stream = this.#streams.get(id)
        if(stream){
            return stream as Readable
        }else{
            throw Exception.create(`Stream with id ${id} not found`).putCode("STREAM_NOT_FOUND")
        }
    }

    static async createReadStreamFromURL(url: string, id: string){
        let def1 = new async.Deferred<IncomingMessage>()
        let mod = http
        if(url.startsWith("https:")) mod = https
        mod.request(url + "?id=" + id, {
            method: 'GET'
        }, async function(response){            
            def1.resolve(response)
        })
            .once("error", def1.reject)
            .end()

        let stream = await def1.promise
        stream["id"] = id 
        return stream as Readable
    }

    static async createWriteStreamFromServer(server: any, id?: string): Promise<Writable>{
        if(!id) id =uniqid()
        let address = server.http?.address
        /*
        if(!address){
            throw Exception.create("Streams are not supported on this server").putCode("NOT_SUPPORTED")
        } */
        
        if(address){
            let url = '', puturl = ''
            if(address?.port){
                url = `http://127.0.0.1:${address.port}/stream/add`
                puturl = `http://127.0.0.1:${address.port}/stream/data`
            }
            else{
                url = `${address}/stream/get`
                puturl = `${address}/stream/data`
            }
            return await this.createWriteStreamFromURLs([url, puturl], id)
        }
        else{
            return this.createWriteStream(id)
        }
    }


    static createWriteStream(id?: string){
        if(!id) id =uniqid()
        let writer = (new PassThrough())
        writer["id"] = id
        RPC.#streams.set(id, writer)
        return writer
    
    }


    static async createWriteStreamFromURLs(urls: Array<string>, id: string){
        let def1 = new async.Deferred<void>()
        let mod = http
        if(urls[0].startsWith("https:")) mod = https
        
        mod.request(urls[0] + "?id=" + id, {
            method: 'PUT'
        },async function(response){
            try{
                let def = new async.Deferred<IncomingMessage>()
                let bytes = Buffer.allocUnsafe(0)
                response.on("data", function(data){
                    bytes = Buffer.concat([bytes, data])
                })
                response.once("end", def.resolve)
                response.once("error", def.resolve)
                await def.promise

                let str = bytes.toString()
                
                let data = JSON.parse(str)
                if(response.statusCode != 200){
                    let error= data?.error 
                    throw  Exception.create(error?.message || "Failed creating stream").putCode(error?.code || "STREAM_FAILED")                    
                }
                def1.resolve()

            }catch(e){
                def1.reject(e)
            }
        }).end()
        await def1.promise
        
        let stream = mod.request(urls[1] + "?id=" + id, {
            method: 'PUT'
        }, async function(response){
            response.on("error", (e) => stream.emit("error", e))
        })
        stream["id"] = id 
        return stream  as Writable
    }




    async createReadStream(id: string, duplex = false): Promise<Readable>{

        if(!id)
            id = uniqid()

        let url = '', puturl = ''
        if(this.channel.meta.type == "client" &&  this.channel.meta.http.enabled){
            //  connect to server 
            url = `${this.channel.meta.http.address}/stream/get`
            puturl = `${this.channel.meta.http.address}/stream/data`
        }
        else if(this.channel.meta.type == "server" &&  this.channel.meta.http.enabled){
            //  connect to server 
            if(this.channel.meta.http.address?.port){
                url = `http://127.0.0.1:${this.channel.meta.http.address.port}/stream/get`
                puturl = `http://127.0.0.1:${this.channel.meta.http.address.port}/stream/data`
            }
            else{
                url = `${this.channel.meta.http.address}/stream/get`
                puturl = `${this.channel.meta.http.address}/stream/data`
            }   
        }
        if(url){
            return await RPC.createReadStreamFromURL(url, id)
        }
        else{
            let client = await Client.connect(this.channel.meta.address as string)
            let rpc = new RPC()
            rpc.channel = client
            rpc.init()
            let remoteRpc = await rpc.defaultScope.remote("__rpc") as RPC
            

            if(await remoteRpc.$connectStreamToSocket(id, duplex) === false){
                throw Exception.create(`Stream with id ${id} not found`).putCode("STREAM_NOT_FOUND")
            }
            rpc.channel.stopRPCMode()
            rpc = null
            client.socket["id"] = id
            return client.socket as Readable
        }
        
    }


    async createWriteStream(id?: string, duplex = false): Promise<Writable>{

        if(!id)
            id = uniqid()

        let url = '', puturl = ''
        if(this.channel.meta.type == "client" &&  this.channel.meta.http.enabled){
            //  connect to server 
            url = `${this.channel.meta.http.address}/stream/add`
            puturl = `${this.channel.meta.http.address}/stream/data`
        }
        else if(this.channel.meta.type == "server" &&  this.channel.meta.http.enabled){
            //  connect to server 
            if(this.channel.meta.http.address?.port){
                url = `http://127.0.0.1:${this.channel.meta.http.address.port}/stream/add`
                puturl = `http://127.0.0.1:${this.channel.meta.http.address.port}/stream/data`
            }
            else{
                url = `${this.channel.meta.http.address}/stream/add`
                puturl = `${this.channel.meta.http.address}/stream/data`
            }
            
        }
        if(url){
            return await RPC.createWriteStreamFromURLs([url, puturl], id)
        }
        else{
            // connect in another socket 
            let client = await Client.connect(this.channel.meta.address as string)
            let rpc = new RPC()
            rpc.channel = client
            rpc.init()
            let remoteRpc = await rpc.defaultScope.remote("__rpc") as RPC

            await remoteRpc.$connectSocketToStream(id, duplex)
            //    throw Exception.create(`Stream with id ${id} not found`).putCode("STREAM_NOT_FOUND")
            //}
            rpc.channel.stopRPCMode()
            rpc = null
            client.socket["id"] = id
            return client.socket as Writable
        }
        
    }





    shared(value: any){
        if(!value || (typeof value != "object" && typeof value != "function")){
            throw Exception.create("Failed share value not an object")
        }
        if(value[SharedValueSymbol] === undefined){
            Object.defineProperty(value, SharedValueSymbol, {
                enumerable: false,
                value: new Shared(value)
            })
            
        }
        return value[SharedValueSymbol]
    }

    $connectStreamToSocket(id: string, duplex = false){
        let stream = RPC.#streams.get(id)
        if(!stream) return false 
        this.channel.stopRPCMode()
        stream.on("error", (e) => this.channel.socket.emit("error",e))
        stream.pipe(this.channel.socket as Socket)
        if(duplex){
            (this.channel.socket as Socket).pipe(stream)
        }
    }

    $connectSocketToStream(id: string, duplex = false){
        let stream = RPC.#streams.get(id)
        if(!stream){
            stream = new PassThrough()
            stream["id"] = id 
            RPC.#streams.set(id, stream)
        }
        this.channel.stopRPCMode()

        let socket = this.channel.socket as Socket
        socket.on("error", (e) => stream.emit("error",e))
        socket.pipe(stream)

        if(duplex){
            stream.pipe(socket)
        }
    }

    init(){
        let def = new Scope(this, "default")
        this.$scopes.set("default", def)
        def.add("__rpc", this)

        
        this.channel.on("message", this.$message.bind(this))
        this.channel.once("close", ()=>{
            let keys = this.$scopes.keys()
            for(let key of keys){
                this.$scopes.get(key).clear()
            }
            this.$scopes.clear()
            delete this.channel
        })

        let self = this

        
        this.channel.jsonOptions.reviver = function(key: string, value: any){
            if(value && (value.type == "ownvariable")){
                return self.get(value.name)
            }

            if(value && (value.type == "sharedobject")){
                let scope = self.$scopes.get(value.scope) || self.defaultScope
                value = scope.$remote(value.name, value.properties)
            }
            return value 
        }
        /*
        this.channel.jsonOptions.replacer = function(key: string, value: any){
            if(value?.type == "sharedobject"){
                value = new Target(value)
                value.init()
            }
        }*/
    }


    props(name: string){
        
        let value = this.get(name)
        if(value)
            return this.$getProps(value)

    }

    $getProps(value: any){
        let properties = new Map<string, any>()
        if(value && typeof value == "object"){
            if(typeof value == "object" && value){


                let allprotos = []
                let proto = Object.getPrototypeOf(value)
                while(proto){
                    allprotos.push(proto)
                    proto = Object.getPrototypeOf(proto)
                }
                allprotos.reverse()
                let props1 = []
                for(let proto of allprotos){
                    let props_ = Object.getOwnPropertyDescriptors(proto)
                    for(let id in props_){
                        props_[id].enumerable = true
                        props1[id] = props_[id]
                    }        
                }


                let props = Object.getOwnPropertyDescriptors(value)
                props = Object.assign(props1, props)
                for(let id in props){
                    if(["__defineGetter__", "__defineSetter__", "__lookupGetter__", "__lookupSetter__", "__proto__"].indexOf(id) >= 0){
                        continue 
                    }
                    
                    let prop = props[id]
                    if(prop.enumerable){
                        if(prop.get){
                            properties.set(id, {
                                type: "property",
                                name: id
                            })
                        }
                        else{
                            if(typeof prop.value == "function"){
                                properties.set(id, {
                                    type: "method",
                                    name: id
                                })
                            }
                            else{
                                properties.set(id, {
                                    type: "field",
                                    name: id
                                })
                            }
                        }
                    }
                }


                if(!(value instanceof Array)){
                    for(let id in value){
                        if(typeof value[id] == "function"){
                            properties.set(id, {
                                type: "method",
                                name: id
                            })
                        }else{
                            properties.set(id, {
                                type: "field",
                                name: id
                            })
                        }
                    }
                }
            }
        }
        return Array.from(properties.values()) // [...properties.values()]
    }


    get(name: string){
        // get data from prop
        let value = null, parts = name.split(".")
        name = parts[0]
        for(let key of this.$scopes.keys()){
            let scope = this.$scopes.get(key)
            if(scope.$vars.has(name)){
                value = scope.get(name)
                break 
            }
        }

        for(let i=1;i<parts.length;i++){
            if(value === undefined){
                throw Exception.create(`Variable ${name} not found`).putCode("INVALID_VARNAME")
            }
            value = value[parts[i]]
        }

        return value 
    }

    call(name: string,  args: Array<any>){
        return this.defaultScope.call(name, args)
    }

    $remote_remove_scope(name: string){
        let scope = this.$scopes.get(name)
        if(scope){
            scope.clear()
        }
    }


    /*
    createScope(){
        let sc= new Scope(this)
        this.$scopes.set(sc.id, sc)
        return sc 
    }*/



    get defaultScope(){
        return this.$scopes.get("default")
    }

    clear(){
        let keys = this.$scopes.keys()
        for(let key of keys){
            this.$scopes.get(key).clear()
        }
        this.$scopes.clear()
    }

    async $message(msg: any){

        if(!msg) return         
        if(msg.type == "req"){

            let res = {
                type: 'res',
                task: msg.task,
                error: undefined,
                data: undefined
            }

            let scope = this.defaultScope
            try{
                
                if(msg.scope){
                    if(!this.$scopes.get(msg.scope)){
                        // create new scope 
                        scope = new Scope(this, msg.scope)
                        this.$scopes.set(msg.scope, scope)
                    }
                }


                let parts = msg.target.split(".")
                let target = null, method = null
                for(let i=0;i<parts.length;i++){
                    let part = parts[i]
                    if(i == 0){
                        // get object 
                        for(let key of this.$scopes.keys()){
                            let scope = this.$scopes.get(key)
                            target = scope.get(part)
                            method = target
                            if(target) break 
                        }
                    }   
                    else{
                        if(!target) break 
                        if(part == "#invoke"){
                            //part = "call"
                            /*
                            let newargs = [null]
                            newargs.push(...msg.args)
                            msg.args = newargs
                            */
                           method = target
                        }
                        else{
                            method = target[part]
                        }
                        if(i == (parts.length - 1)) break 
                        target = method 
                    }
                }

                if(!target){
                    throw Exception.create(`Target ${msg.target} not valid`).putCode ("INVALID_TARGET")
                }

                if(typeof method === "function"){
                    /*
                    if(parts[parts.length - 1] == "#invoke"){
                        // is a direct call 
                        console.info("Before make direct call", msg.args)
                        res.data = await method(...msg.args)
                    }
                    else{
                        */
                    res.data = await method.apply(target, msg.args)
                    
                }
                else{
                    if(parts[parts.length - 1] == "#invoke")
                        throw Exception.create(`Target ${msg.target} cannot be invoke as function`).putCode ("NOT_A_FUNCTION")
                    res.data = method
                }
                res.data = res.data || null

            }catch(e){
                res.error = {
                    message: e.message,
                    code: e.code,
                    stack: e.stack,
                    type: e.type
                }
            }

            if(this.writable)
                await this.channel.send(res, res.data ? scope.$jsonreplacer : undefined)
            scope = null 
            return 
        }

        if(msg.type == "res"){
            for(let key of this.$scopes.keys()){
                let scope = this.$scopes.get(key)
                let task = scope.$tasks.get(msg.task)
                if(task){
                    task.resolve(msg)
                    break 
                }
            }
        }
    }   


    


    get writable(){
        return this.channel && this.channel.writable
    }
}