// emulate a net.Socket from WebSockets
import {w3cwebsocket} from 'npm://websocket@1.0.34'
import { EventEmitter } from 'events'
import Exception from 'gh+/kwruntime/std@1.1.4/util/exception.ts'

export class Socket extends EventEmitter{
	$int = null
	w3c: w3cwebsocket
	constructor(){
		super()
		
	}
	connect(address: string){
		this.w3c = new w3cwebsocket(address, "echo-protocol")
		this.w3c.onopen = ()=> this.emit("connect")
		this.w3c.onerror = (e)=> {
			//console.info("AQUÍ................", e)
			if(!e.message){
				e = Exception.create("Connect error.").putCode("CONNECT_ERROR")
			}
			this.emit("error", e)
		}
		
		this.w3c.onclose = ()=> this.emit("close")
		this.w3c.onmessage = (e)=> {
			this.emit("data", Buffer.from(e.data))
		} 
	}

	write(data){
		this.w3c.send(data)
	}

	end(data){
		if(data)
			this.w3c.send(data)
		
		this.w3c.close()
	}

	// pause and resume not available?
	pause(){}
	resume(){}
	setKeepAlive(enable: boolean, time: number){
		
		if(this.$int){
			clearTimeout(this.$int)
		}
		if(!enable) return 

		if(time > 0){
			time = Math.min(time, 5000)
		}
		let ping = ()=>{
			if(this.destroyed){
				delete this.$int 
				return 
			}
			this.write("{}\n")
			this.$int = setTimeout(ping, time)
		}
		this.$int = setTimeout(ping, time)
	}

	get writable(){
		return this.w3c.readyState == this.w3c.OPEN
	}

	get writableEnded(){
		return this.destroyed
	}

	get destroyed(){
		return this.w3c.readyState == this.w3c.CLOSED
	}

	destroy(){
		this.w3c.close()
	}
}
